# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Company.create(name: 'Axon IT Consulting')
#User.create(first_name: 'Patricia', last_name: 'Navarro', email: 'pnavarro@axon.ph', password: 'password', password_confirmation: 'password', :company_id => '1')
#User.create(first_name: 'Kevin', last_name: 'Obispo', email: 'kobispo@axon.ph', password: 'password', password_confirmation: 'password', :company_id => '1')

User.find(1).update_attributes(first_name: 'Patricia', last_name: 'Navarro')
User.find(2).update_attributes(first_name: 'Kevin', last_name: 'Obispo')