class CreateProductItemPrices < ActiveRecord::Migration
  def change
    create_table :product_item_prices do |t|
      t.float :price_per_unit
      t.boolean :is_current
      
      t.timestamps
    end
  end
end
