class RenameCompanyIndexToCompanyIdInProductItems < ActiveRecord::Migration
  def change
    rename_column :product_items, :company_index, :company_id
  end
end
