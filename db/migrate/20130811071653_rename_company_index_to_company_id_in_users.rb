class RenameCompanyIndexToCompanyIdInUsers < ActiveRecord::Migration
  def change
    rename_column :users, :company_index, :company_id
  end
end
