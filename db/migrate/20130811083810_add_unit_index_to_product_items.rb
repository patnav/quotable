class AddUnitIndexToProductItems < ActiveRecord::Migration
  def change
    add_column :product_items, :unit_id, :integer
    add_index :product_items, :unit_id
  end
end
