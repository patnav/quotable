class CreateProductItems < ActiveRecord::Migration
  def change
    create_table :product_items do |t|
      t.string :name
      t.string :description
      
      t.timestamps
    end
  end
end
