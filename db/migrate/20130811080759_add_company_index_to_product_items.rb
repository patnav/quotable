class AddCompanyIndexToProductItems < ActiveRecord::Migration
  def change
    add_column :product_items, :company_index, :integer
    add_index :product_items, :company_index
  end
end
