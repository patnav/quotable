class AddProductItemIndexToProductItemPrices < ActiveRecord::Migration
  def change
    add_column :product_item_prices, :product_item_id, :integer
    add_index :product_item_prices, :product_item_id
  end
end
