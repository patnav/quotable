class AddCompanyIndexToUsers < ActiveRecord::Migration
  def change
    add_column :users, :company_index, :integer
    add_index :users, :company_index
  end
end
