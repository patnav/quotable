# Flatly
# Bootswatch
# bootswatch.js.coffee
# http://coffeescriptcookbook.com/chapters/syntax/embedding_javascript

#jQuery ->
#  $("a[rel=popover]").popover()
#  $(".tooltip").tooltip()
#  $("a[rel=tooltip]").tooltip()

app = angular.module("Quotable", ["ngResource", "ngGrid"])

$(document).on "ready page:load", ->
  angular.bootstrap document, ["Quotable"]

app.factory "Proposal", ["$resource", ($resource) ->
	$resource("/proposals/:id", {id: "@id"}, { query: {method: "GET", isArray: true}, update: {method: "PUT"}})
]

app.factory "ProductItem", ["$resource", ($resource) ->
	$resource("/product_items/:id", {id: "@id"}, { query: {method: "GET", isArray: true}, save: {method:'POST', isArray: true}, update: {method: "PUT"} })
]

app.factory "Unit", ["$resource", ($resource) ->
	$resource("/units/:id", {id: "@id"}, {update: {method: "PUT"}})
]

app.factory "User", ["$resource", ($resource) ->
	$resource("/users/:id", {id: "@id"}, {update: {method: "PUT"}})
]

app.directive "ckEditor", ->
  require: "?ngModel"
  link: (scope, elm, attr, ngModel) ->
    ck = CKEDITOR.replace(elm[0])
    return  unless ngModel
    ck.on "pasteState", ->
      scope.$apply ->
        ngModel.$setViewValue ck.getData()


    ngModel.$render = (value) ->
      ck.setData ngModel.$viewValue
	  
@UsersCtrl = ["$scope", "User", ($scope, User) ->
  $scope.users = User.query()
]

@ProposalsCtrl = ["$scope", "Proposal", ($scope, Proposal) ->
  $scope.proposals = Proposal.query()
  $scope.showAddProposalContainer = false
  $scope.showAddProposalButton = true
  
  $scope.toggleAddProposalButton = (toggle) ->
	  $scope.showAddProposalButton = toggle
  $scope.toggleAddProposalContainer = (toggle) ->
	  $scope.showAddProposalContainer = toggle
  
  $scope.gridOptions = 
    data: 'proposals',
    columnDefs: [
      field: "name"
      displayName: "Name"
    ]
  
  $scope.addProposal = ->
	  proposal = Proposal.save($scope.newProposal)
	  $scope.newProposal = {}
	  $scope.proposals = Proposal.query()
	  $scope.showAddProposalContainer = false
	  $scope.showAddProposalButton = true
]

@ProductItemsCtrl = ["$scope", "ProductItem", ($scope, ProductItem) ->
  $scope.product_items = ProductItem.query()
  $scope.showAddProductItemContainer = false
  $scope.showAddProductItemButton = true
  
  $scope.toggleAddProductItemButton = (toggle) ->
	  $scope.showAddProductItemButton = toggle
  $scope.toggleAddProductItemContainer = (toggle) ->
	  $scope.showAddProductItemContainer = toggle
	  
  $scope.gridOptions = 
    data: 'product_items',
    columnDefs: [
       field: "name"
       displayName: "Name"
      ,
       field: "description"
       displayName: "Description"
      ,
       field: "unit['name']"
       displayName: "Unit"
    ]
	
  $scope.addProductItem = ->
	  product_item = ProductItem.save($scope.newProductItem)
	  $scope.newProductItem = {}
	  $scope.product_items = ProductItem.query()
	  $scope.showAddProductItemContainer = false
	  $scope.showAddProductItemButton = true
]

@UnitsCtrl = ["$scope", "Unit", ($scope, Unit) ->
  $scope.units = Unit.query()
  
  $scope.gridOptions = 
    data: 'units',
    columnDefs: [
      field: "name"
      displayName: "Name"
    ]
]