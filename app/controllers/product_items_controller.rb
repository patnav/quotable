class ProductItemsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @product_items = ProductItem.all
    
    respond_to do |format|
      format.html
      format.json { render :json => @product_items.to_json(:include => {:unit => {:only => :name}} ) }
      format.xml { render :xml => @product_items }
    end
  end

  def show
    @product_item = ProductItem.find(params[:id])
  end

  def new
    @product_item = ProductItem.new
  end

  def edit
    @product_item = ProductItem.find(params[:id])
  end

  def create
    @product_item = ProductItem.new(params[:product_item])

    if @product_item.save
      flash[:success] = "Product item has been created"
      redirect_to :action => :index
    else
      render :action => :new
    end
  end
  
  def update
    @product_item = ProductItem.find(params[:id])

    if @product_item.save
      flash[:success] = "Product item has been updated"
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @product_item = ProductItem.find(params[:id])
    @product_item.destroy

    flash[:success] = "Product item was successfully deleted"
    redirect_to :action => :index
  end
end
