class ProposalsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @proposals = Proposal.all
    
    respond_to do |format|
      format.html
      format.json { render :json => @proposals }
      format.xml { render :xml => @proposals }
    end
  end

  def show
    @proposal = Proposal.find(params[:id])
  end

  def new
    @proposal = Proposal.new
  end

  def edit
    @proposal = Proposal.find(params[:id])
  end

  def create
    @proposal = Proposal.new(params[:proposal])

    if @proposal.save
      flash[:success] = "Proposal has been created"
      redirect_to :action => :index
    else
      render :action => :new
    end
  end
  
  def update
    @proposal = Proposal.find(params[:id])

    if @proposal.save
      flash[:success] = "Proposal has been updated"
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @proposal = Proposal.find(params[:id])
    @proposal.destroy

    flash[:success] = "Proposal was successfully deleted"
    redirect_to :action => :index
  end
end
