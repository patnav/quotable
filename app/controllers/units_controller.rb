class UnitsController < ApplicationController
  before_filter :authenticate_user!
  
  def index
    @units = Unit.all
    
    respond_to do |format|
      format.html
      format.json { render :json => @units }
      format.xml { render :xml => @units }
    end
  end

  def show
    @unit = Unit.find(params[:id])
  end

  def new
    @unit = Unit.new
  end

  def edit
    @unit = Unit.find(params[:id])
  end

  def create
    @unit = Unit.new(params[:unit])

    if @unit.save
      flash[:success] = "Unit has been created"
      redirect_to :action => :index
    else
      render :action => :new
    end
  end
  
  def update
    @unit = Unit.find(params[:id])

    if @unit.save
      flash[:success] = "Unit has been updated"
      redirect_to :action => :index
    else
      render :action => :edit
    end
  end

  def destroy
    @unit = Unit.find(params[:id])
    @unit.destroy

    flash[:success] = "Unit was successfully deleted"
    redirect_to :action => :index
  end
end
