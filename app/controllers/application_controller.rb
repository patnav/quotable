class ApplicationController < ActionController::Base
  protect_from_forgery
  
  layout "flatly"
  
  after_filter  :set_csrf_cookie_for_ng 
  
protected

  def after_sign_in_path_for(resource)
    dashboard_index_path
  end
  
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end
  
  def verified_request?
    super || form_authenticity_token == request.headers['X_XSRF_TOKEN']
  end
end
