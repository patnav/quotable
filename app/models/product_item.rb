class ProductItem < ActiveRecord::Base
  attr_accessible :name, :description
  attr_accessible :company_id, :unit_id
  
  belongs_to :company
  belongs_to :unit
  
  has_many :product_item_prices
end
