class ProductItemPrice < ActiveRecord::Base
  attr_accessible :price_per_unit, :is_current
  attr_accessible :product_item_id
  
  belongs_to :product_item
end
