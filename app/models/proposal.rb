class Proposal < ActiveRecord::Base
  attr_accessible :name
  attr_accessible :user_id
  
  belongs_to :user
end
